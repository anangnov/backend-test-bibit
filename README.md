# ENVIROMENT
- MYSQL DATABASE
    - HOST = 207.148.70.202
    - USERNAME = nangning
    - PASSWORD = Nangning123!!
    - DATABASE = db_bibit

- API
    - HOST = 207.148.70.202
    - PORT = 8080
    - FULL URL = http://207.148.70.202:8080
    - DOCUMENTATION API = https://documenter.getpostman.com/view/8468660/TzJsfHqd
    - COLLECTION API = https://www.getpostman.com/collections/969cbb91df8663bfdaf1

## 1. Simple Database querying

```
select a.ID, a.UserName, b.UserName as ParentUserName from test as a left join test as b on b.ID = a.Parent
```

- [Table](http://207.148.70.202:8080/table.png)
- [Query](http://207.148.70.202:8080/query.png)


## 2. Please write a small ExpressJS server to search movies from

### Features
- Controller
- Routes
- Model
- Sequelize (ORM)

### Quick Start

Install package modules
        
    npm install

Run application
    
    npm start


## 3. Please refactor the code below to make it more concise, efficient and readable with good logic flow.

```javascript
function findFirstStringInBracket(str) { 
  if (str.length > 0) {
    let indexFirstBracketFound = str.indexOf("("); 
    if (indexFirstBracketFound >= 0) {
      let wordsAfterFirstBracket = str.substr(indexFirstBracketFound); 
      if (wordsAfterFirstBracket) {
        wordsAfterFirstBracket = wordsAfterFirstBracket.substr(1);
        let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")");
        return indexClosingBracketFound >= 0 ? wordsAfterFirstBracket.substring(0, indexClosingBracketFound) : ''
      }
    }
  } else { 
    return '';
  }
}
```


## 4. Logic Test

```javascript
function anagram(arr) {
  let obj = [];
  let anagrams = [];

  for (let i in arr) {
    let word = arr[i];
    let sorting = word.split('').sort().join();
    if (anagrams[sorting] != null) {
        anagrams[sorting].push(word);
    } else {
      anagrams[sorting] = [word];
    }
  }

  for (let i in anagrams) {
    let word = anagrams[i];
    let output = [];

    for (let j = 0; j < word.length; j++) {
      output[j] = word[j]
    }

    obj.push(output)
  }

  return obj
}

let arr = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']

console.log(anagram(arr))
```
