"use strict";

const chai = require("chai");
const assert = require("assert");
const chaiHttp = require("chai-http");
const app = require("../main");

chai.use(chaiHttp);
chai.should();

describe("Search Movie", () => {
  // Run search movie
  describe("POST Search Movie", () => {
    it("should get data search by query", done => {
      chai
        .request(app)
        .post("/api/search")
        .send({ query: "spiderman" })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  // Run detail movie
  describe("GET Detail Movie", () => {
    it("should get data movie by id", done => {
      chai
        .request(app)
        .get("/api/detail/tt2705436")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });
});
