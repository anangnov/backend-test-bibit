// function findFirstStringInBracket(str) {
//   if (str.length > 0) {
//     let indexFirstBracketFound = str.indexOf("(");
//     if (indexFirstBracketFound >= 0) {
//       let wordsAfterFirstBracket = str.substr( indexFirstBracketFound );
//       if (wordsAfterFirstBracket) {
//         wordsAfterFirstBracket = wordsAfterFirstBracket.substr(1);
//         let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")");
//         if (indexClosingBracketFound >= 0) {
//           return wordsAfterFirstBracket.substring(0, indexClosingBracketFound);
//         } else {
//           return '';
//         }
//       } else {
//         return '';
//       }
//     } else {
//       return '';
//     }
//   } else {
//     return '';
//   }
// }

// function findFirstStringInBracket(str) {
//   if (str.length > 0) {
//     let indexFirstBracketFound = str.indexOf("(");
//     if (indexFirstBracketFound >= 0) {
//       let wordsAfterFirstBracket = str.substr(indexFirstBracketFound);
//       if (wordsAfterFirstBracket) {
//         wordsAfterFirstBracket = wordsAfterFirstBracket.substr(1);
//         let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")");
//         return indexClosingBracketFound >= 0 ? wordsAfterFirstBracket.substring(0, indexClosingBracketFound) : ''
//       }
//     }
//   } else {
//     return '';
//   }
// }

// let s = '(alex123'
// console.log(findFirstStringInBracket(s))

function anagram(arr) {
  let obj = [];
  let anagrams = [];

  for (let i in arr) {
    let word = arr[i];
    let sorting = word
      .split("")
      .sort()
      .join();
    if (anagrams[sorting] != null) {
      anagrams[sorting].push(word);
    } else {
      anagrams[sorting] = [word];
    }
  }

  for (let i in anagrams) {
    let word = anagrams[i];
    let output = [];

    for (let j = 0; j < word.length; j++) {
      output[j] = word[j];
    }

    obj.push(output);
  }

  return obj;
}

let arr = ["kita", "atik", "tika", "aku", "kia", "makan", "kua"];

console.log(anagram(arr));
