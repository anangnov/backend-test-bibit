"use-strict";

const unirest = require("unirest");

const search = async (req, res) => {
  let getData = await unirest
    .get(
      `${process.env.API_URL}/?apikey=${process.env.API_KEY}&s=${req.body.query}`
    )
    .headers({
      Accept: "application/json",
      "Content-Type": "application/json"
    });

  let json = JSON.parse(getData.raw_body);
  let objectResponse = await {
    error: false,
    message: "success",
    data: json.Search,
    count: json.totalResults
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const detail = async (req, res) => {
  let getData = await unirest
    .get(
      `${process.env.API_URL}/?apikey=${process.env.API_KEY}&i=${req.params.id}`
    )
    .headers({
      Accept: "application/json",
      "Content-Type": "application/json"
    });

  let json = JSON.parse(getData.raw_body);
  let objectResponse = await {
    error: false,
    message: "success",
    data: json
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { search, detail };
