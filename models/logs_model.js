"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "logs",
    {
      endpoint: {
        type: DataTypes.STRING,
        allowNull: true
      },
      request: {
        type: DataTypes.STRING,
        allowNull: true
      },
      response: {
        type: DataTypes.STRING,
        allowNull: true
      },
      method: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      tableName: "logs",
      timestamps: false
    }
  );
};
