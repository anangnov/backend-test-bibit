"use strict";

const logger = require("nangning-logger-color");
const models = require("../models");

module.exports = (req, res, objectResponse, type, status) => {
  if (type == "info") {
    // Logger info command line
    logger.info({
      ip: req.headers["x-forwarded-for"] || req.connection.remoteAddress,
      endpoint: req.originalUrl,
      params: req.body,
      method: req.method,
      results: objectResponse
    });

    // Insert log to db
    models.logs
      .create({
        endpoint: req.originalUrl.toString(),
        method: req.method.toString(),
        request: JSON.stringify(req.body),
        response: JSON.stringify(objectResponse)
      })
      .then();
  } else if (type == "error") {
    // Logger error command line
    logger.error({
      ip: req.headers["x-forwarded-for"] || req.connection.remoteAddress,
      endpoint: req.originalUrl,
      params: req.body,
      method: req.method,
      results: objectResponse
    });

    // Insert log to db
    models.logs
      .create({
        endpoint: req.originalUrl.toString(),
        method: req.method.toString(),
        request: JSON.stringify(req.body),
        response: JSON.stringify(objectResponse)
      })
      .then();
  }

  status = status === "" ? 200 : status;
  return res.status(status).send(objectResponse);
};
