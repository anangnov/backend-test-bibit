const express = require("express");
const router = express.Router();
const searchMovieController = require("../controller/searchMovieController");

module.exports = app => {
  router.post("/search", searchMovieController.search);
  router.get("/detail/:id", searchMovieController.detail);

  app.use("/api", router);
};
